package com.alpergayretoglu.movie_provider.service;

import com.alpergayretoglu.movie_provider.exception.EntityNotFoundException;
import com.alpergayretoglu.movie_provider.model.entity.Category;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.model.request.category.CategoryUpdateRequest;
import com.alpergayretoglu.movie_provider.repository.CategoryRepository;
import com.alpergayretoglu.movie_provider.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryService {

    private final SecurityService securityService;

    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;

    public Category addCategory(String parentCategoryId, String name, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        Category parent = findCategoryById(parentCategoryId);

        Category category = Category.builder()
                .name(name)
                .isSuperCategory(false)
                .parent(parent)
                .build();

        return categoryRepository.save(category);
    }

    public Page<Category> listCategories(Pageable pageable) {
        return categoryRepository.findAll(pageable);
    }

    public Category findCategoryById(String categoryId) {
        return categoryRepository.findById(categoryId)
                .orElseThrow(() -> new EntityNotFoundException("Category not found with id: " + categoryId + "."));
    }

    public Category findCategoryByName(String categoryName, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExists(authenticatedUserOptional);

        return categoryRepository.findCategoryByName(categoryName)
                .orElseThrow(() -> new EntityNotFoundException("Category not found with name: " + categoryName + "."));
    }

    public Category updateCategory(String categoryId, CategoryUpdateRequest request, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        Category category = findCategoryById(categoryId);

        if (category.isSuperCategory()) {
            throw new RuntimeException("Super category cannot be updated");
        }

        Category newParentCategory = findCategoryByName(request.getParentName(), authenticatedUserOptional);

        if (category.getId().equals(newParentCategory.getId())) {
            throw new RuntimeException("A category cannot be its own parent");
        }

        category.setName(request.getName());
        category.setParent(newParentCategory);
        return categoryRepository.save(category);
    }

    public void deleteCategory(String categoryId, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        Category category = findCategoryById(categoryId);

        if (category.isSuperCategory()) {
            throw new RuntimeException("Super category cannot be deleted");
        }

        // delete category from user favorites
        List<User> users = userRepository.findByFollowedCategories_Id(categoryId);
        for (User user : users) {
            user.getFollowedCategories().remove(category);
            userRepository.save(user);
        }

        // assign all subcategories to grandparent
        Category grandParent = category.getParent();
        for (Category subCategory : category.getSubCategories()) {
            subCategory.setParent(grandParent);
        }

        categoryRepository.delete(category);
    }

}