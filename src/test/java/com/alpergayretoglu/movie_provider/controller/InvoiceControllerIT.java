package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.AbstractIntegrationTest;
import com.alpergayretoglu.movie_provider.model.entity.Invoice;
import com.alpergayretoglu.movie_provider.model.request.payment.PaymentRequest;
import com.alpergayretoglu.movie_provider.model.response.InvoiceResponse;
import com.alpergayretoglu.movie_provider.repository.InvoiceRepository;
import com.alpergayretoglu.movie_provider.service.UserService;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class InvoiceControllerIT extends AbstractIntegrationTest {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private UserService userService;


    @Test
    public void given_amount_and_receiverCard_and_senderCard_when_pay_then_status_code_is_200_and_invoice_is_saved() {
        // Given
        int amount = 40;
        String receiverCard = "123";
        String senderCard = "321";

        PaymentRequest paymentRequest = PaymentRequest.builder()
                .amount(amount)
                .receiverCard(receiverCard)
                .senderCard(senderCard)
                .build();


        // When
        String userId = "member";
        List<Invoice> invoices = userService.listInvoicesForUser(userId, Optional.of(userService.getUserById(userId)));
        Invoice invoice = invoices.get(0);
        String invoiceId = invoice.getId();

        int initialAmount = invoice.getFee();
        int expectedAmount = initialAmount - amount;

        final ResponseEntity<InvoiceResponse> response = sendInvoicePayRequest(userId, invoiceId, paymentRequest);

        // Then
        Invoice updatedInvoice = getInvoiceWithId(invoiceId);
        InvoiceResponse invoiceResponse = response.getBody();

        assertEquals(HttpStatus.OK, response.getStatusCode());

        assertNotNull(invoiceResponse);
        assertEquals(invoiceId, updatedInvoice.getId());
        assertEquals(invoiceId, invoiceResponse.getId());
        assertEquals(expectedAmount, updatedInvoice.getFee());
        assertEquals(expectedAmount, invoiceResponse.getFee());
    }


    // UTILS
    private Invoice getInvoiceWithId(String invoiceId) {
        return invoiceRepository.findById(invoiceId).orElseThrow(
                () -> new RuntimeException("Invoice not found with id: " + invoiceId)
        );
    }

    private ResponseEntity<InvoiceResponse> sendInvoicePayRequest(String userId, String invoiceId, PaymentRequest paymentRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(paymentRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, tokenGenerator.generateJwtHeader(userId));
        return restTemplate.exchange(
                "/invoice/" + invoiceId + "/pay",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<InvoiceResponse>() {
                }
        );
    }
}