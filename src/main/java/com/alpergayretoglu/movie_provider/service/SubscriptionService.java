package com.alpergayretoglu.movie_provider.service;

import com.alpergayretoglu.movie_provider.exception.EntityNotFoundException;
import com.alpergayretoglu.movie_provider.model.entity.Subscription;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.model.request.subscription.SubscriptionCreateRequest;
import com.alpergayretoglu.movie_provider.model.request.subscription.SubscriptionUpdateRequest;
import com.alpergayretoglu.movie_provider.model.response.SubscriptionResponse;
import com.alpergayretoglu.movie_provider.repository.SubscriptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SubscriptionService {

    private final SecurityService securityService;

    private final SubscriptionRepository repository;

    public Page<Subscription> listSubscriptions(Pageable pageable, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExists(authenticatedUserOptional);

        return repository.findAll(pageable);
    }

    public Subscription findById(String id) {
        return repository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public SubscriptionResponse getSubscription(String subId, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExists(authenticatedUserOptional);

        return SubscriptionResponse.fromEntity(findById(subId));
    }

    public Subscription createSubscription(SubscriptionCreateRequest request, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        return repository.save(SubscriptionCreateRequest.toEntity(request));
    }

    public Subscription updateSubscription(String subId, SubscriptionUpdateRequest request, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        Subscription oldSubscription = findById(subId);
        return repository.save(SubscriptionUpdateRequest.updateWith(oldSubscription, request));
    }

    public void deactivateSubscription(String subId, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);
        
        Subscription subscription = findById(subId);
        if (!subscription.isActive()) {
            throw new IllegalArgumentException("Subscription is already inactive");
        }
        subscription.setActive(false);
        repository.save(subscription);
    }
}