package com.alpergayretoglu.movie_provider;


import com.alpergayretoglu.movie_provider.util.TokenGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AbstractIntegrationTest {

    protected final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    protected TestRestTemplate restTemplate;

    @Autowired
    protected TokenGenerator tokenGenerator;

}
