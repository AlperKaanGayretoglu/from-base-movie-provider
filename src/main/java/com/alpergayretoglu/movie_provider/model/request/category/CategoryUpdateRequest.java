package com.alpergayretoglu.movie_provider.model.request.category;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategoryUpdateRequest {
    private String name;
    private String parentName;
}
