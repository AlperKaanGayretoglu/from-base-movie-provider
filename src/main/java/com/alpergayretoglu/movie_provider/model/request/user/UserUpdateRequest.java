package com.alpergayretoglu.movie_provider.model.request.user;

import com.alpergayretoglu.movie_provider.model.entity.User;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class UserUpdateRequest {

    @NotBlank(message = "Name cannot be empty!")
    private String name;

    @NotBlank(message = "Surname cannot be empty!")
    private String surname;

    public static User toEntity(User user, UserUpdateRequest request) {
        user.setName(request.getName());
        user.setSurname(request.getSurname());
        return user;
    }

}
