package com.alpergayretoglu.movie_provider.service;

import com.alpergayretoglu.movie_provider.exception.EntityNotFoundException;
import com.alpergayretoglu.movie_provider.model.entity.FileData;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.repository.FileDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FileService {

    private final SecurityService securityService;

    private final FileDataRepository fileDataRepository;

    @Value("${upload.directory}")
    private String uploadDirectory;

    public FileData storeFile(MultipartFile file, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        String fileName = file.getOriginalFilename();
        if (fileName == null) {
            fileName = "null";
        }

        Path filePath = Paths.get(uploadDirectory).resolve(fileName);

        try {
            Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }

        Optional<FileData> existingFileDataOptional = fileDataRepository.findByUrl(filePath.toString());
        if (existingFileDataOptional.isPresent()) {
            FileData existingFileData = existingFileDataOptional.get();
            existingFileData.setByteSize(file.getSize());

            return fileDataRepository.save(existingFileData);
        }

        FileData newFileData = FileData.builder()
                .name(file.getOriginalFilename())
                .contentType(file.getContentType())
                .byteSize(file.getSize())
                .url(filePath.toString())
                .build();

        return fileDataRepository.save(newFileData);
    }

    public ResponseEntity<Resource> downloadFile(String fileId, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        FileData savedFile = fileDataRepository.findById(fileId).orElseThrow(EntityNotFoundException::new);
        String absolutePath = savedFile.getUrl();

        File file = new File(absolutePath);
        Resource resource = new FileSystemResource(file);

        if (!resource.exists()) throw new EntityNotFoundException();

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    public List<FileData> listFiles(Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        return fileDataRepository.findAll();
    }

}