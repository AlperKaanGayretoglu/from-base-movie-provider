package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.AbstractIntegrationTest;
import com.alpergayretoglu.movie_provider.model.entity.Category;
import com.alpergayretoglu.movie_provider.model.request.category.CategoryCreateRequest;
import com.alpergayretoglu.movie_provider.model.request.category.CategoryUpdateRequest;
import com.alpergayretoglu.movie_provider.model.response.CategoryResponse;
import com.alpergayretoglu.movie_provider.repository.CategoryRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class CategoryControllerIT extends AbstractIntegrationTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void given_category_name_when_add_category_to_parent_then_category_is_added() {
        // Given
        String parentCategoryId = "66403305-972b-42b1-a71a-d7bb2828eebe";
        String categoryName = "ZOMBIE";

        CategoryCreateRequest categoryCreateRequest = new CategoryCreateRequest();
        categoryCreateRequest.setName(categoryName);

        // When
        final ResponseEntity<CategoryResponse> response = sendCategoryCreateRequest(parentCategoryId, categoryCreateRequest);

        // Then
        Category category = getCategoryWithName(categoryName);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(categoryName, category.getName());
    }

    @Test
    public void given_category_name_and_parent_name_when_update_category_then_category_is_updated() {
        // Given
        String categoryId = "50a5fc87-4cbe-4b50-ac5a-acdd90bbfbf6";
        String categoryNewName = "ROMANCE";
        String categoryNewParentName = "SUPER CATEGORY";

        CategoryUpdateRequest categoryUpdateRequest = CategoryUpdateRequest.builder()
                .name(categoryNewName)
                .parentName(categoryNewParentName)
                .build();

        // When
        final ResponseEntity<CategoryResponse> response = sendCategoryUpdateRequest(categoryId, categoryUpdateRequest);

        // Then
        Category category = getCategoryWithId(categoryId);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(categoryNewName, category.getName());
    }

    @Test
    public void given_category_id_when_delete_category_then_category_is_deleted() {
        // Given
        String categoryId = "50a5fc87-4cbe-4b50-ac5a-acdd90bbfbb4";

        // When
        final ResponseEntity<Object> response = sendCategoryDeleteRequest(categoryId);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(categoryRepository.existsById(categoryId));
    }


    // UTILS
    private Category getCategoryWithName(String categoryName) {
        return categoryRepository.findCategoryByName(categoryName).orElseThrow(
                () -> new RuntimeException("Category not found with name: " + categoryName)
        );
    }

    private Category getCategoryWithId(String categoryId) {
        return categoryRepository.findById(categoryId).orElseThrow(
                () -> new RuntimeException("Category not found with id: " + categoryId)
        );
    }

    private ResponseEntity<CategoryResponse> sendCategoryCreateRequest(String parentCategoryId, CategoryCreateRequest categoryCreateRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(categoryCreateRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/category/" + parentCategoryId,
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<CategoryResponse>() {
                }
        );
    }

    private ResponseEntity<CategoryResponse> sendCategoryUpdateRequest(String categoryId, CategoryUpdateRequest categoryUpdateRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(categoryUpdateRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/category/" + categoryId,
                HttpMethod.PUT,
                request,
                new ParameterizedTypeReference<CategoryResponse>() {
                }
        );
    }

    private ResponseEntity<Object> sendCategoryDeleteRequest(String categoryId) {
        final HttpEntity<JsonNode> request = new HttpEntity<>(tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/category/" + categoryId,
                HttpMethod.DELETE,
                request,
                new ParameterizedTypeReference<Object>() {
                }
        );
    }
}
