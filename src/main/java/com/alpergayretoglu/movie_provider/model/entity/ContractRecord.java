package com.alpergayretoglu.movie_provider.model.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContractRecord extends BaseEntity {

    private String name;

    private int monthlyFee;

    private int duration;

    private boolean isActive;

    @OneToOne
    private User user;

    @Builder.Default
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @OneToMany(mappedBy = "contractRecord", fetch = FetchType.EAGER)
    @Builder.Default
    private List<Invoice> invoices = new ArrayList<>();

    public int getRemainingDuration() {
        return duration - invoices.size();
    }
}
