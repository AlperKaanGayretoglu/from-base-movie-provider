package com.alpergayretoglu.movie_provider.util;

import com.alpergayretoglu.movie_provider.security.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TokenGenerator {

    public final JwtService jwtService;

    public HttpHeaders generateJwtHeader(String userId) {
        String token = jwtService.createToken(userId);
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        return headers;
    }

    public HttpHeaders generateJwtHeaderForAdmin() {
        return generateJwtHeader("admin");
    }
}
