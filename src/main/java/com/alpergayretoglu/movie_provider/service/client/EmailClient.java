package com.alpergayretoglu.movie_provider.service.client;

import com.alpergayretoglu.movie_provider.model.entity.User;
import org.springframework.stereotype.Service;


@Service
public class EmailClient {

    public String sendVerificationEmail(User user) {
        return "Verification email sent to " + user.getEmail() + " with verification code: " + user.getVerificationCode();
    }

    public String sendRecoveryEmail(User user) {
        return "Recovery email sent to " + user.getEmail() + " with recovery code: " + user.getRecoveryCode();
    }
}
