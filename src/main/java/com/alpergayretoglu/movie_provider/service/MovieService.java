package com.alpergayretoglu.movie_provider.service;

import com.alpergayretoglu.movie_provider.exception.EntityNotFoundException;
import com.alpergayretoglu.movie_provider.model.entity.Movie;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.model.request.movie.MovieCreateRequest;
import com.alpergayretoglu.movie_provider.model.request.movie.MovieUpdateRequest;
import com.alpergayretoglu.movie_provider.model.response.MovieResponse;
import com.alpergayretoglu.movie_provider.repository.CategoryRepository;
import com.alpergayretoglu.movie_provider.repository.MovieRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class MovieService {

    private final SecurityService securityService;

    private CategoryRepository categoryRepository;
    private MovieRepository movieRepository;

    public void addMovie(Movie movie, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        movieRepository.save(movie);
    }

    public Movie findMovieById(String movieId, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExists(authenticatedUserOptional);

        return movieRepository.findById(movieId).orElseThrow(EntityNotFoundException::new);
    }

    public Page<Movie> listMovies(Pageable pageable, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExists(authenticatedUserOptional);

        return movieRepository.findAll(pageable);
    }

    public MovieResponse getMovie(String movieId, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExists(authenticatedUserOptional);

        return MovieResponse.fromEntity(findMovieById(movieId, authenticatedUserOptional));
    }

    public MovieResponse createMovie(MovieCreateRequest request, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        return MovieResponse.fromEntity(movieRepository.save(MovieCreateRequest.toEntity(request, categoryRepository)));
    }

    public MovieResponse updateMovie(String movieId, MovieUpdateRequest request, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        return MovieResponse.fromEntity(movieRepository.save(MovieUpdateRequest.updateWith(findMovieById(movieId, authenticatedUserOptional), request, categoryRepository)));
    }
}