package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.AbstractIntegrationTest;
import com.alpergayretoglu.movie_provider.model.entity.Movie;
import com.alpergayretoglu.movie_provider.model.request.movie.MovieCreateRequest;
import com.alpergayretoglu.movie_provider.model.request.movie.MovieUpdateRequest;
import com.alpergayretoglu.movie_provider.model.response.MovieResponse;
import com.alpergayretoglu.movie_provider.repository.MovieRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MovieControllerIT extends AbstractIntegrationTest {

    @Autowired
    private MovieRepository movieRepository;

    @Test
    public void given_valid_movieCreateRequest_when_create_movie_then_status_code_is_200_and_movie_is_saved() {
        // Given
        String title = "Ring";
        String description = "description";
        int movieLength = 10;
        List<String> categoryNames = Collections.singletonList("HORROR");

        MovieCreateRequest movieCreateRequest = MovieCreateRequest.builder()
                .title(title)
                .description(description)
                .movieLength(movieLength)
                .categoryNames(categoryNames)
                .build();

        // When
        final ResponseEntity<MovieResponse> response = sendMovieCreateRequest(movieCreateRequest);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());

        MovieResponse movieResponse = response.getBody();
        assertNotNull(movieResponse);

        Movie movie = getMovieWithId(movieResponse.getId());

        assertEquals(title, movie.getTitle());
        assertEquals(description, movie.getDescription());
        assertEquals(movieLength, movie.getMovieLength());
    }

    @Test
    public void given_valid_movieUpdateRequest_when_update_movie_then_status_code_is_200_and_movie_is_updated() {
        // Given
        String movieId = "421450be-91e2-4184-8450-3dcc12a33e64";
        String newTitle = "Matrix 2";
        String newDescription = "description2";
        int newMovieLength = 30;
        List<String> newCategoryNames = Collections.singletonList("ACTION");

        MovieUpdateRequest movieUpdateRequest = MovieUpdateRequest.builder()
                .title(newTitle)
                .description(newDescription)
                .movieLength(newMovieLength)
                .categoryNames(newCategoryNames)
                .build();

        // When
        final ResponseEntity<MovieResponse> response = sendMovieUpdateRequest(movieId, movieUpdateRequest);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());

        MovieResponse movieResponse = response.getBody();
        assertNotNull(movieResponse);

        Movie movie = getMovieWithId(movieId);

        assertEquals(newTitle, movie.getTitle());
        assertEquals(newDescription, movie.getDescription());
        assertEquals(newMovieLength, movie.getMovieLength());
    }


    // UTILS
    private Movie getMovieWithId(String movieId) {
        return movieRepository.findById(movieId).orElseThrow(
                () -> new RuntimeException("Movie not found with id: " + movieId)
        );
    }

    private ResponseEntity<MovieResponse> sendMovieCreateRequest(MovieCreateRequest movieCreateRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(movieCreateRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/movie",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<MovieResponse>() {
                }
        );
    }

    private ResponseEntity<MovieResponse> sendMovieUpdateRequest(String movieId, MovieUpdateRequest movieUpdateRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(movieUpdateRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/movie/" + movieId,
                HttpMethod.PUT,
                request,
                new ParameterizedTypeReference<MovieResponse>() {
                }
        );
    }


}
