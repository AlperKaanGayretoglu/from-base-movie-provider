package com.alpergayretoglu.movie_provider.model.response;

import com.alpergayretoglu.movie_provider.model.entity.FileData;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FileResponse extends BaseResponse {

    private String name;
    private String contentType;
    private long byteSize;
    private String url;

    public static FileResponse fromEntity(FileData file) {
        FileResponse response = FileResponse.builder()
                .name(file.getName())
                .contentType(file.getContentType())
                .byteSize(file.getByteSize())
                .url(file.getUrl())
                .build();
        return setCommonValuesFromEntity(response, file);
    }
}