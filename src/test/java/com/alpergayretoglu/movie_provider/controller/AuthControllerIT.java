package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.AbstractIntegrationTest;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.model.request.auth.login.LoginRequest;
import com.alpergayretoglu.movie_provider.model.request.auth.register.RegisterRequest;
import com.alpergayretoglu.movie_provider.model.response.LoginResponse;
import com.alpergayretoglu.movie_provider.repository.UserRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AuthControllerIT extends AbstractIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void given_email_and_password_when_register_then_status_code_is_200_and_user_is_saved() {
        // Given
        String name = "New Guest";
        String surname = "New Guest";
        String mailAddress = "newguest@mail.com";
        String password = "123456789";

        RegisterRequest registerRequest = RegisterRequest.builder()
                .name(name)
                .surname(surname)
                .email(mailAddress)
                .password(password)
                .build();

        // When
        final ResponseEntity<RegisterRequest> response = sendRegisterRequest(registerRequest);

        // Then
        User user = getUserWithMail(mailAddress);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mailAddress, user.getEmail());
    }

    @Test
    public void given_matching_email_and_password_when_login_then_status_code_is_200_and_return_id_and_token() {
        // Given
        String mailAddress = "guest@mail.com";
        String password = "123456789";

        LoginRequest loginRequest = LoginRequest.builder()
                .email(mailAddress)
                .password(password)
                .build();

        // When
        final ResponseEntity<LoginResponse> response = sendLoginRequest(loginRequest);

        // Then
        User user = getUserWithMail(mailAddress);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(user.getId(), response.getBody().getId());
        assertNotNull(response.getBody().getToken());
    }

    @Test
    public void given_not_matching_email_and_password_when_login_then_status_code_is_409() {
        // Given
        String mailAddress = "guest@mail.com";
        String password = "12345678";

        LoginRequest loginRequest = LoginRequest.builder()
                .email(mailAddress)
                .password(password)
                .build();

        // When
        final ResponseEntity<LoginResponse> response = sendLoginRequest(loginRequest);

        // Then
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
    }


    // UTILS
    private User getUserWithMail(String mailAddress) {
        return userRepository.findByEmail(mailAddress).orElseThrow(
                () -> new RuntimeException("User not found with email: " + mailAddress)
        );
    }

    private ResponseEntity<LoginResponse> sendLoginRequest(LoginRequest loginRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(loginRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, new HttpHeaders());
        return restTemplate.exchange(
                "/auth/login",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<LoginResponse>() {
                });
    }

    private ResponseEntity<RegisterRequest> sendRegisterRequest(RegisterRequest registerRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(registerRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, new HttpHeaders());
        return restTemplate.exchange(
                "/auth/register",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<RegisterRequest>() {
                });
    }

}
