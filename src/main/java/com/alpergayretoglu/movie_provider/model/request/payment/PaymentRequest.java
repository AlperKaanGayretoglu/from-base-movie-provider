package com.alpergayretoglu.movie_provider.model.request.payment;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class PaymentRequest {
    @NotNull
    private int amount;

    @NotBlank
    private String senderCard;

    @NotBlank
    private String receiverCard;

}