package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.model.response.CategoryResponse;
import com.alpergayretoglu.movie_provider.model.response.MovieResponse;
import com.alpergayretoglu.movie_provider.model.response.UserResponse;
import com.alpergayretoglu.movie_provider.service.AuthenticationService;
import com.alpergayretoglu.movie_provider.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/user/{userId}")
@RequiredArgsConstructor
public class UserInterestController {

    private final AuthenticationService authenticationService;
    private final UserService userService;

    @GetMapping("/favorite-movies")
    public Page<MovieResponse> getFavoriteMovies(@PathVariable String userId, @ApiIgnore Pageable pageable) {
        return userService.getFavoriteMovies(userId, pageable, authenticationService.getAuthenticatedUser()).map(MovieResponse::fromEntity);
    }

    @GetMapping("/favorite-movies/{movieId}")
    public MovieResponse getFavoriteMovie(@PathVariable String userId, @PathVariable String movieId) {
        return MovieResponse.fromEntity(userService.getFavoriteMovie(userId, movieId, authenticationService.getAuthenticatedUser()));
    }

    @GetMapping("/followed-categories")
    public Page<CategoryResponse> getFollowedCategories(@PathVariable String userId, @ApiIgnore Pageable pageable) {
        return userService.getFollowedCategories(userId, pageable, authenticationService.getAuthenticatedUser()).map(CategoryResponse::fromEntity);
    }

    @GetMapping("/followed-categories/{categoryId}")
    public CategoryResponse getFollowedCategory(@PathVariable String userId, @PathVariable String categoryId) {
        return CategoryResponse.fromEntity(userService.getFollowedCategory(userId, categoryId, authenticationService.getAuthenticatedUser()));
    }

    @PostMapping("/movie/{movieId}/favorite")
    public UserResponse favoriteMovie(@PathVariable String userId, @PathVariable String movieId) {
        return UserResponse.fromEntity(userService.favoriteMovie(userId, movieId, authenticationService.getAuthenticatedUser()));
    }

    @PostMapping("/movie/{movieId}/unfavorite")
    public UserResponse unfavoriteMovie(@PathVariable String userId, @PathVariable String movieId) {
        return UserResponse.fromEntity(userService.unfavoriteMovie(userId, movieId, authenticationService.getAuthenticatedUser()));
    }

    @PostMapping("/category/{categoryId}/follow")
    public UserResponse followCategory(@PathVariable String userId, @PathVariable String categoryId) {
        return UserResponse.fromEntity(userService.followCategory(userId, categoryId, authenticationService.getAuthenticatedUser()));
    }

    @PostMapping("/category/{categoryId}/unfollow")
    public UserResponse unfollowCategory(@PathVariable String userId, @PathVariable String categoryId) {
        return UserResponse.fromEntity(userService.unfollowCategory(userId, categoryId, authenticationService.getAuthenticatedUser()));
    }
}