package com.alpergayretoglu.movie_provider.service;

import com.alpergayretoglu.movie_provider.exception.BusinessException;
import com.alpergayretoglu.movie_provider.exception.ErrorCode;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.model.enums.UserRole;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class SecurityService {

    public User assertUserExists(Optional<User> userOptional, BusinessException businessException) {
        return userOptional.orElseThrow(() -> businessException);
    }

    public User assertUserExists(Optional<User> userOptional) {
        return assertUserExists(userOptional, new BusinessException(ErrorCode.ACCOUNT_MISSING, "User not found"));
    }

    public User assertUserExistsAndIsAdmin(Optional<User> userOptional, BusinessException businessException) {
        User user = assertUserExists(userOptional);
        if (!user.getUserRole().equals(UserRole.ADMIN)) {
            throw businessException;
        }
        return user;
    }

    public User assertUserExistsAndIsAdmin(Optional<User> userOptional) {
        return assertUserExistsAndIsAdmin(userOptional, new BusinessException(ErrorCode.FORBIDDEN, "User is not admin"));
    }

    public User assertUserExistsAndIsSelf(Optional<User> userOptional, String userId, BusinessException businessException) {
        User user = assertUserExists(userOptional);
        if (!user.getId().equals(userId)) {
            throw businessException;
        }
        return user;
    }

    public User assertUserExistsAndIsSelf(Optional<User> userOptional, String userId) {
        return assertUserExistsAndIsSelf(userOptional, userId, new BusinessException(ErrorCode.FORBIDDEN, "User is not self"));
    }

}