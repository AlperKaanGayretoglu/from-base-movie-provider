package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.AbstractIntegrationTest;
import com.alpergayretoglu.movie_provider.model.entity.Subscription;
import com.alpergayretoglu.movie_provider.model.request.subscription.SubscriptionCreateRequest;
import com.alpergayretoglu.movie_provider.model.request.subscription.SubscriptionUpdateRequest;
import com.alpergayretoglu.movie_provider.model.response.SubscriptionResponse;
import com.alpergayretoglu.movie_provider.repository.SubscriptionRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

public class SubscriptionControllerIT extends AbstractIntegrationTest {

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Test
    public void given_valid_subscriptionCreateRequest_when_create_then_status_code_is_200_and_subscription_is_saved() {
        // Given
        String name = "WOOD";
        int duration = 10;
        int monthlyFee = 10;
        boolean active = true;

        SubscriptionCreateRequest subscriptionCreateRequest = SubscriptionCreateRequest.builder()
                .name(name)
                .duration(duration)
                .monthlyFee(monthlyFee)
                .isActive(active)
                .build();


        // When
        final ResponseEntity<SubscriptionResponse> response = sendSubscriptionCreateRequest(subscriptionCreateRequest);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        SubscriptionResponse subscriptionResponse = response.getBody();
        assertNotNull(subscriptionResponse);

        Subscription subscription = getSubscriptionWithName(name);

        assertEquals(name, subscription.getName());
        assertEquals(duration, subscription.getDuration());
        assertEquals(monthlyFee, subscription.getMonthlyFee());
        assertEquals(active, subscription.isActive());
    }

    @Test
    public void given_valid_subscriptionUpdateRequest_when_update_then_status_code_is_200_and_subscription_is_saved() {
        // Given
        String subscriptionId = "33b455da-335a-4dc1-b4f1-b526c1c9ab4e";

        String name = "SILVER";
        int duration = 3;
        int monthlyFee = 90;
        boolean active = true;

        SubscriptionUpdateRequest subscriptionUpdateRequest = SubscriptionUpdateRequest.builder()
                .name(name)
                .duration(duration)
                .monthlyFee(monthlyFee)
                .isActive(active)
                .build();

        // When
        final ResponseEntity<SubscriptionResponse> response = sendSubscriptionUpdateRequest(subscriptionId, subscriptionUpdateRequest);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        SubscriptionResponse subscriptionResponse = response.getBody();
        assertNotNull(subscriptionResponse);

        Subscription subscription = getSubscriptionWithName(name);

        assertEquals(name, subscription.getName());
        assertEquals(duration, subscription.getDuration());
        assertEquals(monthlyFee, subscription.getMonthlyFee());
        assertEquals(active, subscription.isActive());
    }

    @Test
    public void when_delete_then_status_code_is_200_and_subscription_is_deleted() {
        // Given
        String subscriptionId = "11b455da-715a-4dc1-b4f1-b526c1c9ab4e";

        // When
        final ResponseEntity<Void> response = sendSubscriptionDeleteRequest(subscriptionId);

        // Then
        Subscription subscription = getSubscriptionWithId(subscriptionId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(subscription.isActive());
    }


    // UTILS
    private Subscription getSubscriptionWithName(String name) {
        return subscriptionRepository.findByName(name).orElseThrow(
                () -> new RuntimeException("Subscription not found with name: " + name)
        );
    }

    private Subscription getSubscriptionWithId(String id) {
        return subscriptionRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Subscription not found with id: " + id)
        );
    }

    private ResponseEntity<SubscriptionResponse> sendSubscriptionCreateRequest(SubscriptionCreateRequest subscriptionCreateRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(subscriptionCreateRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/subscription",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<SubscriptionResponse>() {
                }
        );
    }

    private ResponseEntity<SubscriptionResponse> sendSubscriptionUpdateRequest(String subscriptionId, SubscriptionUpdateRequest subscriptionUpdateRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(subscriptionUpdateRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/subscription/" + subscriptionId,
                HttpMethod.PUT,
                request,
                new ParameterizedTypeReference<SubscriptionResponse>() {
                }
        );
    }

    private ResponseEntity<Void> sendSubscriptionDeleteRequest(String subscriptionId) {
        final HttpEntity<Void> request = new HttpEntity<>(tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/subscription/" + subscriptionId,
                HttpMethod.DELETE,
                request,
                new ParameterizedTypeReference<Void>() {
                }
        );
    }
}