package com.alpergayretoglu.movie_provider.model.entity;

import lombok.*;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FileData extends BaseEntity {

    private String name;

    private String url;
    
    private String contentType;

    private long byteSize;


}