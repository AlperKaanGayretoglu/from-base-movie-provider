package com.alpergayretoglu.movie_provider.config;

import com.alpergayretoglu.movie_provider.model.entity.ContractRecord;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.model.enums.UserRole;
import com.alpergayretoglu.movie_provider.service.InvoiceService;
import com.alpergayretoglu.movie_provider.service.UserService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import java.util.Optional;

@Configuration
public class ConfigRunner {
    private final UserService userService;
    private final InvoiceService invoiceService;

    public ConfigRunner(UserService userService, InvoiceService invoiceService) {
        this.userService = userService;
        this.invoiceService = invoiceService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void run() {
        User member = userService.getUserById("member");
        if (member.getUserRole() != UserRole.MEMBER) {
            ContractRecord contractRecord = userService.subscribe(member.getId(), "66b455da-665a-4dc1-b4f1-b526c1c9ab4e", Optional.of(member));
            invoiceService.createInvoice(contractRecord.getId());
        }
    }

}