package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.AbstractIntegrationTest;
import com.alpergayretoglu.movie_provider.model.entity.Category;
import com.alpergayretoglu.movie_provider.model.entity.Movie;
import com.alpergayretoglu.movie_provider.model.response.UserResponse;
import com.alpergayretoglu.movie_provider.repository.CategoryRepository;
import com.alpergayretoglu.movie_provider.repository.MovieRepository;
import com.alpergayretoglu.movie_provider.service.UserService;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UserInterestControllerIT extends AbstractIntegrationTest {

    @Autowired
    private UserService userService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void when_user_favorites_movie_then_status_code_is_200_and_movie_is_added_to_user_favorite_movies() {
        // Given
        String userId = "guest";
        String movieId = "421450be-91e2-4184-8450-3dcc12a33e63";

        List<Movie> oldFavoriteMovies = movieRepository.findByFans_Id(userId);
        assertTrue(oldFavoriteMovies.stream().noneMatch(movie -> movie.getId().equals(movieId)));

        // When
        final ResponseEntity<UserResponse> response = sendFavoriteMovieRequest(userId, movieId);

        // Then
        UserResponse userResponse = response.getBody();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(userResponse);

        List<Movie> favoriteMovies = movieRepository.findByFans_Id(userId);
        assertTrue(favoriteMovies.stream().anyMatch(movie -> movie.getId().equals(movieId)));

        assertEquals(userId, userResponse.getId());
    }

    @Test
    public void when_user_unfavorites_movie_then_status_code_is_200_and_movie_is_removed_from_user_favorite_movies() {
        // Given
        String userId = "guest";
        String movieId = "b4dceb23-d2ea-4432-aa7a-c71b4b15bcee";

        List<Movie> oldFavoriteMovies = movieRepository.findByFans_Id(userId);
        assertTrue(oldFavoriteMovies.stream().anyMatch(movie -> movie.getId().equals(movieId)));

        // When
        final ResponseEntity<UserResponse> response = sendUnfavoriteMovieRequest(userId, movieId);

        // Then
        UserResponse userResponse = response.getBody();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(userResponse);

        List<Movie> favoriteMovies = movieRepository.findByFans_Id(userId);
        assertFalse(favoriteMovies.stream().anyMatch(movie -> movie.getId().equals(movieId)));

        assertEquals(userId, userResponse.getId());
    }

    @Test
    public void when_user_follows_category_then_status_code_is_200_and_category_is_added_to_user_followed_categories() {
        // Given
        String userId = "guest";
        String categoryId = "50a5fc87-4cbe-4b50-ac5a-acdd90bbfbf5";

        List<Category> oldFollowedCategories = categoryRepository.findByFollowers_Id(userId);
        assertTrue(oldFollowedCategories.stream().noneMatch(category -> category.getId().equals(categoryId)));

        // When
        final ResponseEntity<UserResponse> response = sendFollowCategoryRequest(userId, categoryId);

        // Then
        UserResponse userResponse = response.getBody();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(userResponse);

        List<Category> followedCategories = categoryRepository.findByFollowers_Id(userId);
        assertTrue(followedCategories.stream().anyMatch(category -> category.getId().equals(categoryId)));

        assertEquals(userId, userResponse.getId());
    }

    @Test
    public void when_user_unfollows_category_then_status_code_is_200_and_category_is_removed_from_user_followed_categories() {
        // Given
        String userId = "guest";
        String categoryId = "66403305-972b-42b1-a71a-d7bb2828eebe";

        List<Category> oldFollowedCategories = categoryRepository.findByFollowers_Id(userId);
        assertTrue(oldFollowedCategories.stream().anyMatch(category -> category.getId().equals(categoryId)));

        // When
        final ResponseEntity<UserResponse> response = sendUnfollowCategoryRequest(userId, categoryId);

        // Then
        UserResponse userResponse = response.getBody();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(userResponse);

        List<Category> followedCategories = categoryRepository.findByFollowers_Id(userId);
        assertFalse(followedCategories.stream().anyMatch(category -> category.getId().equals(categoryId)));

        assertEquals(userId, userResponse.getId());
    }


    // UTILS
    private ResponseEntity<UserResponse> sendFavoriteMovieRequest(String userId, String movieId) {
        final HttpEntity<JsonNode> request = new HttpEntity<>(tokenGenerator.generateJwtHeader(userId));
        return restTemplate.exchange(
                "/user/" + userId + "/movie/" + movieId + "/favorite",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<UserResponse>() {
                }
        );
    }

    private ResponseEntity<UserResponse> sendUnfavoriteMovieRequest(String userId, String movieId) {
        final HttpEntity<JsonNode> request = new HttpEntity<>(tokenGenerator.generateJwtHeader(userId));
        return restTemplate.exchange(
                "/user/" + userId + "/movie/" + movieId + "/unfavorite",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<UserResponse>() {
                }
        );
    }

    private ResponseEntity<UserResponse> sendFollowCategoryRequest(String userId, String categoryId) {
        final HttpEntity<JsonNode> request = new HttpEntity<>(tokenGenerator.generateJwtHeader(userId));
        return restTemplate.exchange(
                "/user/" + userId + "/category/" + categoryId + "/follow",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<UserResponse>() {
                }
        );
    }

    private ResponseEntity<UserResponse> sendUnfollowCategoryRequest(String userId, String categoryId) {
        final HttpEntity<JsonNode> request = new HttpEntity<>(tokenGenerator.generateJwtHeader(userId));
        return restTemplate.exchange(
                "/user/" + userId + "/category/" + categoryId + "/unfollow",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<UserResponse>() {
                }
        );
    }
}
