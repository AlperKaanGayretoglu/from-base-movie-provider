package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.model.response.FileResponse;
import com.alpergayretoglu.movie_provider.service.AuthenticationService;
import com.alpergayretoglu.movie_provider.service.FileService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/file")
@AllArgsConstructor
public class FileController {

    private final AuthenticationService authenticationService;
    private FileService fileService;

    @GetMapping
    public List<FileResponse> listFiles() {
        return fileService.listFiles(authenticationService.getAuthenticatedUser()).stream().map(FileResponse::fromEntity).collect(Collectors.toList());
    }

    @PostMapping("/upload")
    public FileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        return FileResponse.fromEntity(fileService.storeFile(file, authenticationService.getAuthenticatedUser()));
    }

    @GetMapping("/download/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) {
        return fileService.downloadFile(fileId, authenticationService.getAuthenticatedUser());
    }
}