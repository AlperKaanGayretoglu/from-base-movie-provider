package com.alpergayretoglu.movie_provider.service;

import com.alpergayretoglu.movie_provider.exception.BusinessException;
import com.alpergayretoglu.movie_provider.exception.EntityNotFoundException;
import com.alpergayretoglu.movie_provider.exception.ErrorCode;
import com.alpergayretoglu.movie_provider.model.entity.ContractRecord;
import com.alpergayretoglu.movie_provider.model.entity.Invoice;
import com.alpergayretoglu.movie_provider.model.entity.Payment;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.model.request.payment.PaymentRequest;
import com.alpergayretoglu.movie_provider.model.response.InvoiceResponse;
import com.alpergayretoglu.movie_provider.repository.ContractRecordRepository;
import com.alpergayretoglu.movie_provider.repository.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InvoiceService {

    private final SecurityService securityService;
    private final PaymentService paymentService;

    private final ContractRecordRepository contractRecordRepository;
    private final InvoiceRepository invoiceRepository;

    public Page<Invoice> listInvoices(Pageable pageable, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);
        
        return invoiceRepository.findAll(pageable);
    }

    public Invoice createInvoice(String contractRecordId) {
        ContractRecord contractRecord = contractRecordRepository.findById(contractRecordId)
                .orElseThrow(EntityNotFoundException::new);

        if (contractRecord.getRemainingDuration() <= 0) {
            throw new IllegalStateException("Contract record has no remaining duration");
        }

        return invoiceRepository.save(Invoice.builder()
                .fee(contractRecord.getMonthlyFee())
                .contractRecord(contractRecord)
                .build());
    }

    public Invoice getInvoice(String id, Optional<User> authenticatedUserOptional) {
        securityService.assertUserExistsAndIsAdmin(authenticatedUserOptional);

        return invoiceRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public Invoice payInvoice(String invoiceId, PaymentRequest paymentRequest, Optional<User> authenticatedUserOptional) {
        Invoice invoice = invoiceRepository.findById(invoiceId).orElseThrow(EntityNotFoundException::new);

        User ownerOfInvoice = invoice.getContractRecord().getUser();

        User user = securityService.assertUserExistsAndIsSelf(
                authenticatedUserOptional,
                ownerOfInvoice.getId(),
                new BusinessException(ErrorCode.UNAUTHORIZED, "Invoice does not belong to user"));

        invoice.pay(paymentRequest.getAmount());

        Payment payment = Payment.builder()
                .amount(paymentRequest.getAmount())
                .invoice(invoice)
                .senderCard(paymentRequest.getSenderCard())
                .receiverCard(paymentRequest.getReceiverCard())
                .build();

        paymentService.addPayment(payment);
        return invoice;
    }

    public Invoice findById(String id) {
        return invoiceRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public Page<InvoiceResponse> listInvoicesForContractRecords(List<ContractRecord> contractRecords, Pageable pageable) {
        return invoiceRepository.findAllByContractRecordIn(contractRecords, pageable).map(InvoiceResponse::fromEntity);
    }

    public List<Invoice> listInvoicesForContractRecords(List<ContractRecord> contractRecords) {
        return invoiceRepository.findAllByContractRecordIn(contractRecords);
    }
}