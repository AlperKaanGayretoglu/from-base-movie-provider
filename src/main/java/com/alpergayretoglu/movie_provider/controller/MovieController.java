package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.model.request.movie.MovieCreateRequest;
import com.alpergayretoglu.movie_provider.model.request.movie.MovieUpdateRequest;
import com.alpergayretoglu.movie_provider.model.response.MovieResponse;
import com.alpergayretoglu.movie_provider.service.AuthenticationService;
import com.alpergayretoglu.movie_provider.service.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/movie")
@RequiredArgsConstructor
public class MovieController {

    private final AuthenticationService authenticationService;
    private final MovieService movieService;

    @ApiPageable
    @GetMapping
    public Page<MovieResponse> listMovies(@ApiIgnore Pageable pageable) {
        return movieService.listMovies(pageable, authenticationService.getAuthenticatedUser()).map(MovieResponse::fromEntity);
    }

    @GetMapping("/{movieId}")
    public MovieResponse getMovie(@PathVariable String movieId) {
        return movieService.getMovie(movieId, authenticationService.getAuthenticatedUser());
    }

    @PostMapping
    public MovieResponse createMovie(@RequestBody MovieCreateRequest request) {
        return movieService.createMovie(request, authenticationService.getAuthenticatedUser());
    }

    @PutMapping("/{movieId}")
    public MovieResponse updateMovie(@PathVariable String movieId, @RequestBody MovieUpdateRequest request) {
        return movieService.updateMovie(movieId, request, authenticationService.getAuthenticatedUser());
    }

}