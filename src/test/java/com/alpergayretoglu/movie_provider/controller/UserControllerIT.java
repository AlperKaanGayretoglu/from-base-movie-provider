package com.alpergayretoglu.movie_provider.controller;

import com.alpergayretoglu.movie_provider.AbstractIntegrationTest;
import com.alpergayretoglu.movie_provider.model.entity.User;
import com.alpergayretoglu.movie_provider.model.request.auth.ResetPasswordRequest;
import com.alpergayretoglu.movie_provider.model.request.user.UserUpdateRequest;
import com.alpergayretoglu.movie_provider.model.response.SubscriptionResponse;
import com.alpergayretoglu.movie_provider.model.response.UserResponse;
import com.alpergayretoglu.movie_provider.repository.UserRepository;
import com.alpergayretoglu.movie_provider.util.PasswordChecker;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

public class UserControllerIT extends AbstractIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordChecker passwordChecker;

    @Test
    public void given_name_and_surname_when_update_user_then_status_code_is_200_and_user_is_updated() {
        // Given
        String userId = "guest";
        String newName = "New Name Guest";
        String newSurname = "New Surname Guest";

        UserUpdateRequest userUpdateRequest = UserUpdateRequest.builder()
                .name(newName)
                .surname(newSurname)
                .build();

        // When
        final ResponseEntity<UserResponse> response = sendUserUpdateRequest(userId, userUpdateRequest);

        // Then
        User user = getUserWithId(userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(newName, user.getName());
        assertEquals(newSurname, user.getSurname());
    }

    @Test
    public void when_delete_user_then_status_code_is_200_and_user_is_deleted() {
        // Given
        String userId = "guest2";

        // When
        final ResponseEntity<UserResponse> response = sendUserDeleteRequest(userId);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(userRepository.existsById(userId));
    }

    @Test
    public void when_subscribe_user_then_status_code_is_200_and_user_is_subscribed() {
        // Given
        String userId = "guest";
        String subscriptionId = "66b455da-665a-4dc1-b4f1-b526c1c9ab4e";
        String subscriptionName = "DIAMOND";

        // When
        final ResponseEntity<SubscriptionResponse> response = sendUserSubscribeRequest(userId, subscriptionId);

        // Then
        User user = getUserWithId(userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(subscriptionName, user.getSubscription().getName());
    }

    @Test
    public void given_valid_reset_password_request_when_reset_password_then_status_code_is_200() {
        // Given
        String userId = "guest";
        String oldPassword = "123456789";
        String newPassword = "1234567890";

        ResetPasswordRequest resetPasswordRequest = ResetPasswordRequest.builder()
                .oldPassword(oldPassword)
                .newPassword(newPassword)
                .build();

        // When
        final ResponseEntity<Object> response = sendUserResetPasswordRequest(userId, resetPasswordRequest);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(passwordChecker.checkPassword(newPassword, getUserWithId(userId).getPassword()));
    }


    // UTILS
    private User getUserWithId(String userId) {
        return userRepository.findById(userId).orElseThrow(
                () -> new RuntimeException("User not found with id: " + userId)
        );
    }

    private ResponseEntity<UserResponse> sendUserUpdateRequest(String userId, UserUpdateRequest userUpdateRequest) {
        JsonNode requestBodyJson = objectMapper.valueToTree(userUpdateRequest);
        final HttpEntity<JsonNode> request = new HttpEntity<>(requestBodyJson, tokenGenerator.generateJwtHeaderForAdmin());
        return restTemplate.exchange(
                "/user/" + userId,
                HttpMethod.PUT,
                request,
                new ParameterizedTypeReference<UserResponse>() {
                });
    }

    private ResponseEntity<UserResponse> sendUserDeleteRequest(String userId) {
        return restTemplate.exchange(
                "/user/" + userId,
                HttpMethod.DELETE,
                new HttpEntity<>(tokenGenerator.generateJwtHeaderForAdmin()),
                new ParameterizedTypeReference<UserResponse>() {
                });
    }

    private ResponseEntity<SubscriptionResponse> sendUserSubscribeRequest(String userId, String subscriptionId) {
        return restTemplate.exchange(
                "/user/" + userId + "/subscribe/" + subscriptionId,
                HttpMethod.POST,
                new HttpEntity<>(tokenGenerator.generateJwtHeader(userId)),
                new ParameterizedTypeReference<SubscriptionResponse>() {
                });
    }

    private ResponseEntity<Object> sendUserResetPasswordRequest(String userId, ResetPasswordRequest resetPasswordRequest) {
        return restTemplate.exchange(
                "/user/reset-password",
                HttpMethod.POST,
                new HttpEntity<>(resetPasswordRequest, tokenGenerator.generateJwtHeader(userId)),
                new ParameterizedTypeReference<Object>() {
                });
    }
}
